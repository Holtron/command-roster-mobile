﻿using commandroster.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace commandroster.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}